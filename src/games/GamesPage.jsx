// import React, { Fragment, useState } from "react";
// import "../styles/Main.css";
// import { MdOutlineNavigateNext, MdOutlineNavigateBefore } from "react-icons/md";
// import Ps5Dark from "../assets/Ps5.png";
// import Footer from "../components/Footer";
// import Games from "../components/games"; // Ensure this is correctly imported
// import { useGoogleLogin } from "@react-oauth/google";

// export default function Main() {
//   const [indexOf, setIndexOf] = useState(0); // Initial index to 0

//   const login = useGoogleLogin({
//     onSuccess: (tokenResponse) => console.log(tokenResponse),
//   });

//   return (
//     <Fragment>
//       <div className="container">
//         <div className="Bg-Container-dark">
//           {Games.length > 0 && (
//             <>
//               <img src={Games[indexOf].thumbnail} alt="game" id="Image" />
//               <h2 style={{ color: "#EE9B00" }}>{Games[indexOf].title}</h2>

//               <button
//                 onClick={() => login()}
//                 className="bg-[#EE9B00] font-bold text-gray-100 mt-4 w-[200px] sm:p-4 p-2 rounded-lg text-xl"
//               >
//                 Buy Now
//               </button>
//             </>
//           )}

//           <div>

//                  <MdOutlineNavigateBefore
//             size={100}
//             cursor="pointer"
//             onClick={() => setIndexOf((prev) => Math.max(prev - 1, 0))}
//             className={indexOf <= 0 ? "disable" : "active"}
//             id="BeforeOne"
//           />
//           <MdOutlineNavigateNext
//             size={100}
//             cursor="pointer"
//             onClick={() =>
//               setIndexOf((prev) => Math.min(prev + 1, Games.length - 1))
//             }
//             className={indexOf >= Games.length - 1 ? "disable" : "active"}
//             id="AfterOne"
//           />

//           <div>
//             Lorem Ipsum is simply dummy text of the printing and typesetting
//             industry. Lorem Ipsum has been the industry's standard dummy text
//             ever since the 1500s, when an unknown printer took a galley of type
//             and scrambled it to make a type specimen book. It has survived not
//             only five centuries, but also the leap into electronic typesetting,
//             remaining essentially unchanged. It was popularised in the 1960s
//             with the release of Letraset sheets containing Lorem Ipsum passages,
//             and more recently with desktop publishing software like Aldus
//             PageMaker including versions of Lorem Ipsum.
//           </div>

//           </div>

//         </div>

//         <div className="">
//           <img src={Ps5Dark} alt="ps5" id="Ps5" />
//         </div>
//       </div>
//       <div className="mt-4 p-2">
//         <Footer />
//       </div>
//     </Fragment>
//   );
// }

import React, { Fragment, useState } from "react";
import { MdOutlineNavigateNext, MdOutlineNavigateBefore } from "react-icons/md";
import Ps5Dark from "../assets/Ps5.png";
import Footer from "../components/Footer";
import Games from "../components/games"; // Ensure this is correctly imported
import { useGoogleLogin } from "@react-oauth/google";
import axios from "axios";

export default function Main() {
  const [indexOf, setIndexOf] = useState(0); // Initial index to 0

  const login = useGoogleLogin({
    // onSuccess: (credentialResponse) => {
    //   console.log(credentialResponse);
    //   var credentialsDecoded = jwtDecode(
    //     credentialResponse.credential
    //   );
    //   console.log(credentialsDecoded);
    onSuccess: async (response) => {
      try {
        const res = await axios.get(
          "https://www.googleapis.com/oauth2/v3/userinfo",
          {
            headers: {
              Authorization: `Bearer ${response.access_token}`,
            },
          }
        );

        console.log(res);

        window.location.href = "https://buy.stripe.com/test_eVa8yc3hz8C2c4UeUU";

      } catch (err) {
        console.log(err);
      }
    },
  });

  return (
    <Fragment>
      <div className="flex flex-col items-center">
        <div className="bg-gray-900 p-5 flex flex-col items-center">
          {Games.length > 0 && (
            <div className="flex flex-row items-start gap-5 ">
              <div>
                <img
                  src={Games[indexOf].thumbnail}
                  alt="game"
                  className="max-w-xs"
                />

                {/* <div className="relative">
                  <img
                    src={backgroundImage}
                    alt="Your Image"
                    className="w-full h-auto object-cover"
                  />
                  <div className="absolute inset-0 flex items-center justify-center">
                    <p className="text-white text-3xl">$40.99</p>
                  </div>
                </div> */}

                {/* <p className="text-3xl">$40.99</p> */}
                <p class="text-3xl font-bold	">
                  <span class="align-top text-xl">$</span>
                  <span class="align-middle">{Games[indexOf].price}</span>
                </p>
              </div>

              <div className="flex flex-col  max-w-xl  justify-center	">
                <h2 className="text-[#EE9B00] text-3xl	">
                  {Games[indexOf].title}
                </h2>

                <p className="text-white mt-4">
                  Lorem Ipsum is simply dummy text of the printing and
                  typesetting industry. Lorem Ipsum has been the industry's
                  standard dummy text ever since the 1500s, when an unknown
                  printer took a galley of type and scrambled it to make a type
                  specimen book. It has survived not only five centuries, but
                  also the leap into electronic typesetting, remaining
                  essentially unchanged. It was popularised in the 1960s with
                  the release of Letraset sheets containing Lorem Ipsum
                  passages, and more recently with desktop publishing software
                  like Aldus PageMaker including versions of Lorem Ipsum.
                </p>

                {/* <button
                  onClick={() => login()}
                  className="bg-[#EE9B00] font-bold text-gray-100 mt-4 w-48 p-4 rounded-lg text-xl"
                >
                  Buy Now
                </button> */}

                <button
                  onClick={() => login()}
                  // href="https://buy.stripe.com/test_eVa8yc3hz8C2c4UeUU"
                  className="bg-[#EE9B00] font-bold text-gray-100 mt-4 w-48 p-4 rounded-lg text-xl ml-[200px]"
                >
                  Buy Now
                </button>
              </div>
            </div>
          )}

          <div className="flex justify-center mt-5">
            <MdOutlineNavigateBefore
              size={100}
              cursor="pointer"
              onClick={() => setIndexOf((prev) => Math.max(prev - 1, 0))}
              className={`${
                indexOf <= 0 ? "opacity-50 pointer-events-none" : "opacity-100"
              }`}
            />
            <MdOutlineNavigateNext
              size={100}
              cursor="pointer"
              onClick={() =>
                setIndexOf((prev) => Math.min(prev + 1, Games.length - 1))
              }
              className={`${
                indexOf >= Games.length - 1
                  ? "opacity-50 pointer-events-none"
                  : "opacity-100"
              }`}
            />
          </div>
        </div>

        {/* <div className="mt-5">
          <img src={Ps5Dark} alt="ps5" className="max-w-full" />
        </div> */}
      </div>
      <div className="mt-4 p-2">
        <Footer />
      </div>
    </Fragment>
  );
}
