import axios from "axios";
import React, { Fragment, useEffect, useState } from "react";
import "../styles/BestPriceGame.css";

function BestPriceGames() {
  const Api = "https://free-to-play-games-database.p.rapidapi.com/api/games";
  const [getData, setGetData] = useState([]);

  const ApiCall = async () => {
    try {
      const response = await axios.get(Api, {
        params: { pageSize: "30", id: "452" },
        headers: {
          "X-RapidAPI-Key":
            "9fa85261a5msh2820c1c0d1b6b30p1d2e07jsn718d1a1e937c",
          "X-RapidAPI-Host": "free-to-play-games-database.p.rapidapi.com",
        },
      });

      // Log the entire response for debugging
      console.log(response);

      const data = response.data;

      // Check if data is an array before setting it in state
      if (Array.isArray(data)) {
        setGetData(data);
      } else {
        console.error("API response is not an array:", data);
      }
    } catch (error) {
      console.error("Error fetching data:", error);
    }
  };

  useEffect(() => {
    ApiCall();
  }, []);

  return (
    <Fragment>
      <div className="Game-container">
        {getData.map((game, index) => (
          <div key={index} className="Imagecontainer">
            <img src={game.thumbnail} alt="image" />
            <h3
              style={
                game.title.length >= 34
                  ? { fontSize: "14px", marginTop: "5px", width: "200px" }
                  : null
              }
            >
              {game.title}
            </h3>
            <h4 style={{ color: "#EE9B00" }}>{game.genre}</h4>
          </div>
        ))}
      </div>
    </Fragment>
  );
}

export default BestPriceGames;
