import axios from "axios";
import React, { Fragment, useEffect, useReducer, useState } from "react";
import "../styles/SpecialGames.css";
import { MdOutlineNavigateNext, MdOutlineNavigateBefore } from "react-icons/md";
import GameInfo from "../assets/game6.png";

function SpecialGames() {
  const Api = "https://free-to-play-games-database.p.rapidapi.com/api/games";
  const [games, setGames] = useState([]);
  const [slider, setSlider] = useState(false);
  const [toggle, setToggle] = useState(false);
  const Starting = {
    Title: "",
    Thumbnail: "",
    Publisher: "",
    Genre: "",
    Developer: "",
    Url: "",
  };
  const GameShown = (game) => {
    dispatch({ type: "Click", payload: game });
    setToggle(true);
    console.log(Starting);
  };

  const reducer = (state, action) => {
    switch (action.type) {
      case "Click":
        return {
          Title: (Starting.Title = action.payload.title),
          Thumbnail: (Starting.Thumbnail = action.payload.thumbnail),
          Publisher: (Starting.Publisher = action.payload.Publisher),
          Genre: (Starting.Genre = action.payload.genre),
          Developer: (Starting.Developer = action.payload.developer),
          Url: (Starting.Url = action.payload.game_url),
        };
      default:
        return state;
    }
  };
  const [state, dispatch] = useReducer(reducer, Starting);

  const ApiCall = async () => {
    try {
      const response = await axios.get(Api, {
        params: { page: "1", limit: "10" },

        headers: {
          "X-RapidAPI-Key":
            "9fa85261a5msh2820c1c0d1b6b30p1d2e07jsn718d1a1e937c",
          "X-RapidAPI-Host": "free-to-play-games-database.p.rapidapi.com",
        },
      });
      // console.log(response.data);
      setGames(response.data);
    } catch (error) {
      console.error("Error fetching data:", error);
    }
  };

  useEffect(() => {
    ApiCall();
  }, []);

  return (
    <Fragment>
      <div className="game-container">
        {games.map((game, index) => (
          <div key={index} className="Container">
            <img
              src={game.thumbnail}
              alt="image"
              id={slider ? "image-slider" : "images"}
              onClick={() => GameShown(game)}
            />
            <div className="Title">
              <h3
                id={slider ? "moveTitle" : "Title"}
                style={
                  game.title.length >= 40
                    ? { fonSize: "15px", marginTop: "19px" }
                    : { fontSize: "18px" }
                }
              >
                {game.title}
              </h3>
              <h4
                style={{ color: "#EE9B00" }}
                id={slider ? "movePrice" : "Price"}
              >{`${game.genre} ${game.publisher}`}</h4>
            </div>
          </div>
        ))}
      </div>
      <MdOutlineNavigateBefore
        id={slider ? "prev" : "DisabledPrev"}
        size={65}
        cursor="pointer"
        onClick={() => setSlider(false)}
      />
      <MdOutlineNavigateNext
        id={slider ? "DisabledNext" : "Next"}
        size={65}
        cursor="pointer"
        onClick={() => setSlider(true)}
      />
      {toggle && Starting.Thumbnail ? (
        <GameInfo Data={Starting} setToggle={toggle} />
      ) : null}
    </Fragment>
  );
}

export default SpecialGames;
