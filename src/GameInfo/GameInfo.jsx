import React from "react";
import "../styles/GameInfo.css";
import { AiOutlineCloseCircle } from "react-icons/ai";
import { FaShoppingCart } from "react-icons/fa";
import { Fragment } from "react";

function GameInfo({ Data, setToggle }) {
  return (
    <Fragment>
      <div className="Background">
        <div className="Game-Container">
          <img src={Data.Thumbnail} alt="image" />
          <div className="Game-Info">
            <AiOutlineCloseCircle
              color="#fff"
              size={30}
              cursor="pointer"
              id="Close"
              onClick={() => setToggle(false)}
            />
            <h1
              id="GameTitle"
              style={Data.length > 27 ? { fontSize: "1.5rem" } : {}}
            >
              Game: {Data.Title}
            </h1>

            <h3>Publisher:{Data.Developer}</h3>
            <a href={Data.Url}>
              <span id="buy">
                Buy Now <FaShoppingCart size={30} id="cart" />
              </span>
            </a>
          </div>
        </div>
      </div>
    </Fragment>
  );
}

export default GameInfo;
