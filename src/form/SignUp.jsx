import React from "react";
import gaming from "../assets/gaming1.jpg";
import Footer from "../components/Footer";

export default function SignUp() {
  return (
    <div className="min-h-screen py-40 bg-gradient-to-r from-gray-700 to-[#EE9B00]">
      <div className=" mx-auto">
        <div className="flex flex-col lg:flex-row w-10/12 lg:w-8/12 bg-white rounded-xl mx-auto shadow-lg overflow-hidden">
          <div className="w-full lg:w-1/2 flex-col items-center justify-center p-12 bg-no-repeat bg-center">
            <img
              className=" w-full object-cover object-left scale-x-[-1]"
              src={gaming}
              alt="/"
            />
            <div>
              <h1 className="text-gray-500 text-3xl mb-3">Welcome</h1>
              <p class="text-gray-500">
                Welcome! Start your adventure by signing up.{" "}
                {/* <a href="#" class="text-[#EE9B00] font-semibold">
                  Learn more
                </a> */}
              </p>
            </div>
          </div>
          <div class="w-full lg:w-1/2 py-16 px-12">
            <h2 class="text-3xl mb-4 text-gray-500">Sign Up</h2>
            <p class="mb-4 text-gray-500">
              Create your account. It’s free and only take a minute
            </p>
            <form action="#">
              <div class="grid grid-cols-2 gap-5">
                <input
                  type="text"
                  placeholder="Firstname"
                  class="border border-gray-400 py-1 px-2"
                />
                <input
                  type="text"
                  placeholder="Surname"
                  class="border border-gray-400 py-1 px-2"
                />
              </div>
              <div class="mt-5">
                <input
                  type="text"
                  placeholder="Email"
                  class="border border-gray-400 py-1 px-2 w-full"
                />
              </div>
              <div class="mt-5">
                <input
                  type="password"
                  placeholder="Password"
                  class="border border-gray-400 py-1 px-2 w-full"
                />
              </div>
              <div class="mt-5">
                <input
                  type="password"
                  placeholder="Confirm Password"
                  class="border border-gray-400 py-1 px-2 w-full"
                />
              </div>
              <div class="flex mt-5 gap-2">
                <input type="checkbox" class="border border-gray-400" />
                <span className="text-gray-600">
                  I accept the{" "}
                  <a href="#" class="text-[#EE9B00] font-semibold">
                    Terms of Use
                  </a>{" "}
                  &{" "}
                  <a href="#" class="text-[#EE9B00] font-semibold">
                    Privacy Policy
                  </a>
                </span>
              </div>
              <div class="mt-5">
                <button class="w-full bg-[#EE9B00] py-3 text-center text-gray-100">
                  Register Now
                </button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  );
}
