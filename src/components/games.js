import ExampleGame from "../assets/game.jpg";
import ExampleGame2 from "../assets/game3.jpeg";



const Games = [
  {
    id: 540,
    title: "Friend or Foe?",
    thumbnail: ExampleGame,
    short_description:
      "A hero-focused first-person team shooter from Blizzard Entertainment.",
    game_url: "https://www.freetogame.com/open/overwatch-2",
    genre: "Shooter",
    platform: "PC (Windows)",
    publisher: "We Have the Power Gaming",
    developer: "We Have The Power Gaming",
    release_date: "2022-10-04",
    price: 40.99
    // freetogame_profile_url: "https://www.freetogame.com/overwatch-2",
  },

  {
    id: 540,
    title: "Black Widow",
    thumbnail: ExampleGame2,
    short_description:
      "A hero-focused first-person team shooter from Blizzard Entertainment.",
    game_url: "https://www.freetogame.com/open/overwatch-2",
    genre: "Shooter",
    platform: "PC (Windows)",
    publisher: "We Have the Power Gaming",
    developer: "We Have The Power Gaming",
    release_date: "2022-10-04",
    price: 30.99
    // freetogame_profile_url: "https://www.freetogame.com/overwatch-2",
  },
];

export default Games;
