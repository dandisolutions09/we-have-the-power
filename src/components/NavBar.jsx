import React, { Fragment, useState } from "react";
import { Link, NavLink, Outlet } from "react-router-dom";
import { AiOutlineClose } from "react-icons/ai";
import { BiMenuAltRight } from "react-icons/bi";

export const navLinks = [
  {
    id: "home",
    title: "Home",
  },
  {
    id: "gaming",
    title: "Games",
  },
  {
    id: "about",
    title: "About",
  },
  {
    id: "contact",
    title: "Contact",
  },
  {
    id: "faq",
    title: "FAQ",
  },
  // {
  //   id: "gaming",
  //   title: "Gaming",
  // },
];

export default function NavBar() {
  const [active, setActive] = useState("Home");
  const [toggle, setToggle] = useState(false);

  return (
    <>
      <nav className="flex flex-row m-0 py-8 justify-between items-center bg-[#444040] p-12 w-full navbar">
        <Link
          to="/home"
          className="cursor-pointer hover:scale-110 ease-in duration-200 mr-20 lg:items-start items-center"
        >
          <span className="text-[#FFF] font-bold sm:text-6xl text-4xl">
            We Have{" "}
          </span>
          <span className="text-[#EE9B00] font-bold sm:text-6xl text-4xl">
            The{" "}
          </span>
          <span className="text-[#EE9B00] font-bold sm:text-6xl text-4xl">
            Power
          </span>
        </Link>
        <div className="flex flex-row ">
          <div className="flex flex-row gap-4">
            {/* Desktop navigation */}
            {navLinks.map((nav) => (
              <NavLink
                key={nav.id}
                to={`/${nav.id.toLowerCase()}`}
                className={`list-none sm:flex hidden justify-end items-center flex-1 hover:scale-110 ease-in duration-200 hover:text-[#EE9B00] text-2xl ${
                  active === nav.title ? "text-[#EE9B00]" : "text-dimWhite"
                } ${nav === navLinks.length - 1 ? "mr-0" : "mr-10"}`}
                onClick={() => setActive(nav.title)}
              >
                {nav.title}
              </NavLink>
            ))}
          </div>

          {/* Mobile navigation */}
          <div
            onClick={() => setToggle(!toggle)}
            className="sm:hidden flex flex-1 justify-end items-center"
          >
            {toggle ? (
              <AiOutlineClose size={20} cursor="pointer" />
            ) : (
              <BiMenuAltRight size={20} cursor="pointer" />
            )}
          </div>

          {/* side navigation */}
          <div
            className={`${
              !toggle ? "hidden" : "flex"
            } p-6 bg-gradient-to-r from-gray-400 to-[#444343] absolute top-20 right-0 mx-4 my-6 min-w-[140px] sidebar z-40 border-0`}
          >
            <ul className="list-none flex justify-end items-start flex-1 flex-col gap-6">
              {navLinks.map((nav) => (
                <li
                  key={nav.id}
                  className={`font-poppins font-medium cursor-pointer text-16px ${
                    active === nav.title ? "text-[#EE9B00]" : "text-gray-300"
                  }`}
                  onClick={() => {
                    setActive(nav.title);
                    setToggle(false); // Close the mobile menu on click
                  }}
                >
                  <NavLink
                    to={`/${nav.id.toLowerCase()}`}
                    activeClassName="text-white" // Add activeClassName for active links
                  >
                    {nav.title}
                  </NavLink>
                </li>
              ))}
            </ul>
          </div>
        </div>
      </nav>
      <main>
        <Outlet />
      </main>
    </>
  );
}
