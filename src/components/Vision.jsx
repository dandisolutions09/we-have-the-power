import React from 'react'
import game8 from "../assets/game8.jpg";

export default function Vision() {
  return (
    <div
      style={{ backgroundImage: `url(${game8})`, objectFit: "cover" }}
      className="category1 flex flex-col p-12 m-2 gap-4 justify-center text-center "
    >
      <div className="head cursor-pointer sm:w-[15%] p-4 flex flex-row justify-center text-center bg-[#EE9B00]">
        <h1 className="sm:text-2xl text-xl">Learn more</h1>
      </div>
      <div className="sentence1 sm:w-[500px] w-[390] justify-center text-start bg-[#EE9B00] p-12">
        <p className="text2 sm:">
          Lorem ipsum dolor sit amet consectetur adipisicing elit. Ea esse a
          itaque quia error. Harum dolore quasi quisquam, deserunt illum quae
          nemo debitis pariatur dolorem blanditiis, quibusdam voluptatum illo?
          Dolore qui voluptatibus, libero aut assumenda corrupti quam odit iure
          eos. Velit repellat corporis eaque est! Molestias temporibus
          asperiores dolores quas.
        </p>
      </div>
    </div>
  );
}
