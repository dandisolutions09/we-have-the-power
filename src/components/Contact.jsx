import React from 'react'
import Footer from './Footer';

export default function Contact() {
  return (
    <div class="flex justify-center items-center h-screen bg-gradient-to-r from-gray-700 to-[#EE9B00]">
      <div class="sm:w-[600px] w-96 p-4 mt-12 shadow-lg bg-white rounded-md">
        <h1 class="text-3xl block text-center font-semibold">
          <i class="fa-solid fa-user"></i> Login
        </h1>
        <hr class="mt-3" />
        <div class="mt-3">
          <label
            for="username"
            class="block text-base mb-2 text-gray-500 sm:text-xl"
          >
            Name
          </label>
          <input
            type="text"
            id="username"
            class="border w-full text-base px-4 py-4 focus:outline-none focus:ring-0 focus:border-gray-600 text-gray-500"
            placeholder="Enter name..."
          />
        </div>
        <div class="mt-3">
          <label
            for="username"
            class="block text-base mb-2 text-gray-500 sm:text-xl"
          >
            Email
          </label>
          <input
            type="text"
            id="username"
            class=" border w-full text-base px-4 py-4  focus:outline-none focus:ring-0 focus:border-gray-600 text-gray-500"
            placeholder="Enter email..."
          />
        </div>
        <div class="mt-3">
          <label
            for="password"
            class="block text-base mb-2 text-gray-500 sm:text-xl"
          >
            Phone
          </label>
          <input
            type="number"
            id="password"
            class="border w-full text-base px-4 py-4  focus:outline-none focus:ring-0 focus:border-gray-600 text-gray-500"
            placeholder="Enter phone..."
          />
        </div>
        <div className="flex flex-col py-2">
          <label className="uppercase text-sm py-2">Message</label>
          <textarea
            className="border-2 rounded-lg p-3 border-gray-300"
            name="message"
            rows="4"
            placeholder="write your message..."
          ></textarea>
        </div>
        {/* <div class="mt-3 flex justify-between items-center">
          <div className="flex gap-1">
            <input type="checkbox" />
            <label className="text-gray-500 sm:text-xl">Remember Me</label>
          </div>
          <div>
            <a href="#" class="text-[#EE9B00] font-semibold sm:text-xl">
              Forgot Password?
            </a>
          </div>
        </div> */}
        <div class="mt-5">
          <button
            type="submit"
            class="border-2 border-[#EE9B00] bg-[#EE9B00] text-white py-2 w-full rounded-md hover:bg-transparent hover:text-[#EE9B00] font-semibold sm:text-xl"
          >
            <i class="fa-solid fa-right-to-bracket "></i>&nbsp;&nbsp;Send Message
          </button>
        </div>
      </div>
 
    </div>
  );
}
