import React from "react";
import { BiLogoFacebookCircle, BiLogoTwitch } from "react-icons/bi";
import { BsInstagram, BsLinkedin, BsYoutube } from "react-icons/bs";
import { AiFillInstagram, AiFillTwitterCircle } from "react-icons/ai";
export default function Footer() {
  return (
    <div
      style={{
        background: "black",
        color: "white",
        width: "100%",
        height: "10%",
      }}
    >
      <div>
        <div
          style={{
            display: "flex",
            flexDirection: "row",
            listStyle: "none",
            gap: "18px",
            marginLeft: "1.3rem",
            padding: 10,
          }}
          classNam="footer-list"
        >
          <li className="hover:text-[#EE9B00]">privacy Policy</li>
          <li className="hover:text-[#EE9B00]">cookie Policy</li>
          <li className="hover:text-[#EE9B00]">Store</li>
          <li className="hover:text-[#EE9B00]">Support</li>
          <li className="hover:text-[#EE9B00]">Forum</li>
          <li className="hover:text-[#EE9B00]">Store</li>
        </div>
        <p
          style={{
            marginLeft: "1.7rem",
   
            fontWeight: "lighter",
            opacity: 0.4,
            textDecoration: "underline",
          }}
        >
          &#169; 2023 All Rights Reserved
        </p>
        <div
          className="sm:mr-2 mr-4 pb-2"
          style={{ float: "right", marginBottom: "2rem" }}
        >
          <span
            id="LogoName1"
            className="sm:text-3xl text-xl"
            // style={themeSwitch ? { color: "#3a86ff" } : null}
          >
            We Have
          </span>
          <span id="LogoName2" className="sm:text-3xl text-xl text-[#EE9B00]">
            {" "}
            The
          </span>
          <span id="LogoName2" className="sm:text-3xl text-xl text-[#EE9B00]">
            {" "}
            Power
          </span>
        </div>

        <div
          //   style={{
          //     display: "flex",
          //     flexDirection: "row",
          //     justifyContent: "center",
          //     alignItems: "center",
          //     gap: 20,
          //   }}
          className="flex flex-row justify-center text-center m-2 sm:gap-6 gap-2 p-4"
        >
          <BiLogoFacebookCircle
            style={{ cursor: "pointer", opacity: 0.4 }}
            className="icon1 sm:text-[35px] text-[20px]"
          />
          <BsLinkedin
            size={30}
            style={{ cursor: "pointer", opacity: 0.4 }}
            className="icon1 sm:text-[35px] text-[20px]"
          />
          <AiFillTwitterCircle
            size={30}
            style={{ cursor: "pointer", opacity: 0.4 }}
            className="icon1 sm:text-[35px] text-[20px]"
          />
          <BsYoutube
            size={30}
            style={{ cursor: "pointer", opacity: 0.4 }}
            className="icon1 sm:text-[35px] text-[20px]"
          />
          <BsInstagram
            size={30}
            style={{ cursor: "pointer", opacity: 0.4 }}
            className="icon1 sm:text-[35px] text-[20px]"
          />
          <BiLogoTwitch
            size={30}
            style={{ cursor: "pointer", opacity: 0.4 }}
            className="icon1 sm:text-[35px] text-[20px]"
          />
        </div>
      </div>
    </div>
  );
}
