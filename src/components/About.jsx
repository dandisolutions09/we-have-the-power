import React from "react";
import { BsFacebook } from "react-icons/bs";
import { AiOutlineMail } from "react-icons/ai";
import { BiLogoFacebookCircle, BiLogoTwitch } from "react-icons/bi";
import { BsInstagram, BsLinkedin, BsYoutube } from "react-icons/bs";
import { AiFillInstagram, AiFillTwitterCircle } from "react-icons/ai";
import { useNavigate } from "react-router-dom";
import Footer from "./Footer"
import Vision from "./Vision"
import game from "../assets/game8.jpg"

export default function About() {
  const transition = { type: "spring", duration: 3 };
  const mobile = window.innerWidth <= 768 ? true : false;
  const navigate = useNavigate();


  const handleSignupClick = () => {

    navigate("/home/sign"); 
  };

  return (
    <div className="">
      {/* <div className="bg-cover bg-center hero m-4">
        <span className="absolute right-6 flex gap-4 mt-2 p-4 py-4">
          <BiLogoFacebookCircle
            style={{ cursor: "pointer", opacity: 0.4 }}
            className="icon1 sm:text-[35px] text-[20px] hover:text-[#EE9B00]"
          />
          <BsLinkedin
            size={30}
            style={{ cursor: "pointer", opacity: 0.4 }}
            className="icon1 sm:text-[35px] text-[20px] hover:text-[#EE9B00]"
          />
          <AiFillTwitterCircle
            size={30}
            style={{ cursor: "pointer", opacity: 0.4 }}
            className="icon1 sm:text-[35px] text-[20px] hover:text-[#EE9B00]"
          />
          <BsYoutube
            size={30}
            style={{ cursor: "pointer", opacity: 0.4 }}
            className="icon1 sm:text-[35px] text-[20px] hover:text-[#EE9B00]"
          />
          <BsInstagram
            size={30}
            style={{ cursor: "pointer", opacity: 0.4 }}
            className="icon1 sm:text-[35px] text-[20px] hover:text-[#EE9B00]"
          />
          <BiLogoTwitch
            size={30}
            style={{ cursor: "pointer", opacity: 0.4 }}
            className="icon1 sm:text-[35px] text-[20px] hover:text-[#EE9B00]"
          />
        </span>

        <span className=" flex p-4 py-12 justify-center">
          <h2 className="sm:text-5xl text-4xl p-1">
            About <span className="text-[#EE9B00]">Us</span>
          </h2>
        </span>
        <span className=" flex flex-col m-10 text-center justify-center">
          <h3 className="sm:text-5xl text-3xl pb-2">
            A space for passion and creativity
          </h3>
          <p className="sm:text-3xl text-1xl pt-4 text-gray-300 text-start">
            In the heart of Michigan, a family-owned endeavor took root, driven
            by our shared passion for gaming. Frustrated by the industry's
            status quo, we set out to rewrite the rules. Our vision is crystal
            clear: to revolutionize gaming by placing players at the forefront.
            Quality games, artistic excellence over spreadsheets, and respect
            for gamers' time and investments are our guiding principles. Beyond
            gaming, we're committed to making a positive impact through our
            charity initiatives. Welcome to our gaming community, where
            excitement meets purpose.
          </p>
        </span>
        <div className="flex flex-row space-x-10 sm:gap-8 gap-6 m-4 px-6 text-center justify-center ">
          <div className="sm:flex-col flex max-w-[1040px] md:pl-20 sm:py-1 justify-center text-center gap-6">
            <label className="uppercase text-sm py-2">Email</label>
            <input
              className="border-2 p-2 rounded-lg flex border-gray-100 text-gray-700 focus:outline-none"
              type="text"
              name="phone"
            />
            <button
              className="bg-[#EE9B00] text-gray-100 mt-4 w-full sm:p-4 p-2 rounded-lg"
              onClick={handleSignupClick}
            >
              Signup
            </button>
          </div>
        </div>
      </div> */}
      {/* <div className="absolute h-12 bg-black justify-center text-center w-full">
        <div className="r-0 h-1 w-2 bg-slate-50 justify-center text-center"></div>
      </div> */}
      <div className="grid sm:grid-cols-2 sm:ml-8 ml-2">
        <div className="flex flex-col text4 bg-black sm:w-[600px] p-8 m-5 justify-center text-start mt-10">
          <h1 className="uppercase sm:text-5xl text-4xl border-r-8 border-r-[#EE9B00] text-[#EE9B00] p-2">
            Our Team
          </h1>
          <h5 className="sm:text-2xl text-1xl pt-4 text-gray-300 p-4">
            "We are storytellers at heart, weaving narratives brimming with
            impactful choices and real consequences, all set against the
            backdrop of characters players can genuinely relate to. Whether
            we're crafting sprawling open-world RPGs or intense competitive card
            games, our commitment remains the same: to produce experiences that
            captivate, provide a sense of accomplishment, and uphold the
            principles of fairness and integrity."
          </h5>
        </div>
        <div className="h-[400px] w-[500px] mt-4">
          <img src={game} alt="" />
        </div>
      </div>
      <div className="grid sm:grid-cols-2 sm:ml-8 ml-2">
        <div className="Team  flex flex-col sm:w-[500px] w-[390] m-2 right-4 justify-center text-start">
          <span className="stroke-zinc-600 text-gray-500 sm:text-5xl text-4xl">
            +5{" "}
          </span>
          <span className="text9 text-black uppercase sm:text-4xl text-4xl">
            Members{" "}
          </span>
          <span>
            <p className="text10 sm:text-xl text-l pt-4 text-gray-600">
              Our members are the lifeblood of our gaming community. They're not
              just players; they're passionate individuals who go above and
              beyond to enhance your gaming journey. With an unwavering
              commitment to excellence, they eagerly provide feedback, support,
              and camaraderie to make every gaming moment memorable. From
              offering tips and strategies to creating a welcoming and vibrant
              atmosphere, our members are always ready to share their knowledge
              and enthusiasm. They understand that the true value of gaming lies
              in the shared experiences and connections it fosters. As a result,
              they're ever-prepared to make your gaming experience not just
              enjoyable, but truly worth your time and effort.
            </p>
          </span>
        </div>
        <div className="flex flex-col sm:w-[500px] w-[390] m-2 mt-10 justify-center text-start">
          <h1 className="text8 uppercase bold sm:text-4xl text-4xl  text-[#EE9B00] p-2">
            QUALITY FULL SCALE
          </h1>
          <div className="sentence ">
            <p className="text1 text10 sm:text-xl text-l pt-4 text-gray-600">
              "We firmly believe that settling for 'good enough' is never the
              path to success. Our commitment to quality extends beyond just
              eliminating bugs in our games; it encompasses every aspect of the
              gaming experience, including the creation of in-game assets. We
              understand that achieving true quality takes time and dedication,
              and we firmly reject the notion that game development can be
              reduced to mere spreadsheet calculations. We see our mission as
              game developers as a matter of honoring the trust gamers place in
              us with their time and hard-earned money. To us, it's not just
              about delivering functional games; it's about filling their
              screens with content that excites, inspires, and stirs emotions.
              We continually challenge ourselves to reach new heights of
              artistic excellence in our craft."
            </p>
          </div>
        </div>
      </div>
      <div style={{ marginTop: "8rem" }}>
        <Vision />
      </div>
      <Footer />
    </div>
  );
}
