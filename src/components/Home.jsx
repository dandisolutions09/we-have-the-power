import React from "react";
import Footer from "./Footer";
import { BsFacebook } from "react-icons/bs";
import { AiOutlineMail } from "react-icons/ai";
import { BiLogoFacebookCircle, BiLogoTwitch } from "react-icons/bi";
import { BsInstagram, BsLinkedin, BsYoutube } from "react-icons/bs";
import { AiFillInstagram, AiFillTwitterCircle } from "react-icons/ai";
import { Link } from "react-router-dom";
// import { GoogleLogin } from "@react-oauth/google";
import { useGoogleLogin } from "@react-oauth/google";

import { jwtDecode } from "jwt-decode";

export default function Home({ navigation }) {
  const login = useGoogleLogin({
    onSuccess: (tokenResponse) => console.log(tokenResponse),
  });
  return (
    <div>
      <div className="bg-cover bg-center hero m-4">
        <span className="absolute right-6 flex gap-4 mt-2 p-4 py-4">
          <BiLogoFacebookCircle
            style={{ cursor: "pointer", opacity: 0.4 }}
            className="icon1 sm:text-[35px] text-[20px] hover:text-[#EE9B00]"
          />
          <BsLinkedin
            size={30}
            style={{ cursor: "pointer", opacity: 0.4 }}
            className="icon1 sm:text-[35px] text-[20px] hover:text-[#EE9B00]"
          />
          <AiFillTwitterCircle
            size={30}
            style={{ cursor: "pointer", opacity: 0.4 }}
            className="icon1 sm:text-[35px] text-[20px] hover:text-[#EE9B00]"
          />
          <BsYoutube
            size={30}
            style={{ cursor: "pointer", opacity: 0.4 }}
            className="icon1 sm:text-[35px] text-[20px] hover:text-[#EE9B00]"
          />
          <BsInstagram
            size={30}
            style={{ cursor: "pointer", opacity: 0.4 }}
            className="icon1 sm:text-[35px] text-[20px] hover:text-[#EE9B00]"
          />
          <BiLogoTwitch
            size={30}
            style={{ cursor: "pointer", opacity: 0.4 }}
            className="icon1 sm:text-[35px] text-[20px] hover:text-[#EE9B00]"
          />
        </span>

        {/* <span className=" flex p-4 py-12 justify-center">
        <h2 className="sm:text-5xl text-4xl p-1">
            Welcome To <span className="text-[#EE9B00]">We Have The Power</span>
          </h2> 
        </span> */}
        <span className="py-20  flex flex-col m-10 text-center justify-center">
          <h3 className="sm:text-7xl text-3xl pb-2 font-bold">
            A space for passion and creativity
          </h3>
          <p className="sm:text-3xl text-1xl pt-4 text-gray-300 justify-center text-start mt-8 ">
            In the heart of Michigan, a family-owned endeavor took root, driven
            by our shared passion for gaming. Frustrated by the industry's
            status quo, we set out to rewrite the rules. Our vision is crystal
            clear: to revolutionize gaming by placing players at the forefront.
            Quality games, artistic excellence over spreadsheets, and respect
            for gamers' time and investments are our guiding principles. Beyond
            gaming, we're committed to making a positive impact through our
            charity initiatives. Welcome to our gaming community, where
            excitement meets purpose.
          </p>
        </span>
        <div className="flex flex-row m-4 px-6 text-center justify-center">
          <div className="sm:flex-col flex max-w-[1040px] md:pl-20 sm:py-2 justify-center text-center gap-6 sm:mr-12">
            {/* <label className="uppercase text-sm py-2">Email</label>
            <input
              className="border-2 p-1 rounded-lg flex border-gray-100 text-gray-700 focus:outline-none"
              type="text"
              name="phone"
            /> */}
            <Link
              to="gaming"
              // onClick={() => login()}
              
              className="bg-[#EE9B00] font-bold text-gray-100 mt-4 w-[200px] sm:p-4 p-2 rounded-lg text-xl"
            >
              Our Games
            </Link>
            {/* <GoogleLogin
              onSuccess={(credentialResponse) => {
                console.log(credentialResponse);
                var credentialsDecoded = jwtDecode(
                  credentialResponse.credential
                );
                console.log(credentialsDecoded);
              }}
              onError={() => {
                console.log("Login Failed");
              }}
            /> */}



                  </div>
        </div>
      </div>

      <Footer />
    </div>
  );
}
